package com.get.contact;

import java.util.ArrayList;
import java.util.List;

import com.example.R;


import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Email;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.provider.ContactsContract.CommonDataKinds.StructuredPostal;
import android.provider.ContactsContract.Contacts;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Toast;

public class MyActivity extends Activity {

    private static final String TAG = MyActivity.class.getSimpleName();
    private static final int REQUEST_CODE_PICK_CONTACTS = 1;
    private Uri uriContact;
    private String contactID;     // contacts unique ID


    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);
    }

    public void onClickSelectContact(View btnSelectContact) {

        // using native contacts selection
        // Intent.ACTION_PICK = Pick an item from the data, returning what was selected.
        startActivityForResult(new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI), REQUEST_CODE_PICK_CONTACTS);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == REQUEST_CODE_PICK_CONTACTS && resultCode == RESULT_OK) {
            Log.d(TAG, "Response: " + data.toString());
            uriContact = data.getData();
            
            readContacts();

        }
    }
    
    public void readContacts(){
    	  ContentResolver cr = getContentResolver();
 	     // getting contacts ID
 	        Cursor cursorID = getContentResolver().query(uriContact,
 	                new String[]{ContactsContract.Contacts._ID},
 	                null, null, null);

 	        if (cursorID.moveToFirst()) {

 	            contactID = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID));
 	        }

 	        cursorID.close();


  
 	                   // Get the phone number
 	                   Cursor pCur = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = ?", new String[]{contactID}, null);
 	                   while (pCur.moveToNext()) {
 	                	   	 int phoneType = pCur.getInt(pCur.getColumnIndex(Phone.TYPE));
 	                	     if (phoneType == Phone.TYPE_MOBILE){
 	                	    	 String mobile = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA));
 	                	    	 System.out.println("Mobile: " + mobile);
 		                         //cMobile.setText(mobile);
 	                	     }
 	                	     if (phoneType == Phone.TYPE_WORK || phoneType == Phone.TYPE_HOME || phoneType == Phone.TYPE_MAIN){
 	                	    	 String phone = pCur.getString(pCur.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DATA));
 	                	    	 System.out.println("Phone: " + phone);
 	                	    	 //cPhone.setText(phone);
 	                	     }         
 	                   }
 	                   pCur.close();

 	                   // Get email and type
 	                   Cursor emailCur = cr.query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?", new String[]{contactID}, null);
 	                   final List<String> where = new ArrayList<String>();
 	                   String email = "";
 	                    while (emailCur.moveToNext()) {
 	                        email = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
 	                        String emailType = emailCur.getString(emailCur.getColumnIndex(ContactsContract.CommonDataKinds.Email.TYPE));
 	                        where.add(email);
 	                    }
 	                    emailCur.close();
 	                    String[] simpleArray = new String[ where.size() ];
 	                    where.toArray( simpleArray );
 	                    if(where.size() > 1){
 	                 	   AlertDialog.Builder builder = new AlertDialog.Builder(this);
 	                        builder.setTitle("Make your selection");
 	                        builder.setItems(simpleArray
 	                     		   , new DialogInterface.OnClickListener() {
 	                            public void onClick(DialogInterface dialog, int item) {
 	                         	   System.out.println("Email: " + where.get(item).toString());
 	                         	  //cEmail.setText(where.get(item).toString());
 	                            }
 	                        });
 	                        AlertDialog alert = builder.create();
 	                        alert.show();
 	                    }else{
 	                 	   System.out.println("Email: " + email);
 	                 	   //cEmail.setText(email);
 	                    }

 	                   // Get note.......
 	                   String noteWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
 	                   String[] noteWhereParams = new String[]{contactID,ContactsContract.CommonDataKinds.Note.CONTENT_ITEM_TYPE};
 	                   Cursor noteCur = cr.query(ContactsContract.Data.CONTENT_URI, null, noteWhere, noteWhereParams, null);
 	                   if (noteCur.moveToFirst()) {
 	                       String note = noteCur.getString(noteCur.getColumnIndex(ContactsContract.CommonDataKinds.Note.NOTE));
 	                       System.out.println("Note " + note);
 	                       //cNotes.setText(note);
 	                   }
 	                   noteCur.close();

 	                //Get Postal Address....
 	                   String addrWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
 	                   String[] addrWhereParams = new String[]{contactID, ContactsContract.CommonDataKinds.StructuredPostal.CONTENT_ITEM_TYPE};
 	                   Cursor addrCur = cr.query(ContactsContract.Data.CONTENT_URI,  null, addrWhere, addrWhereParams, null);
 	                   while(addrCur.moveToNext()){
 	                	   String name = addrCur.getString(
 	                               addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.DISPLAY_NAME));
 	                	   String  Street = addrCur.getString(
 	                               addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.STREET));
 	                	   String  Postcode = addrCur.getString(
 	                               addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.POSTCODE));
 	                	   String  City = addrCur.getString(
 	                               addrCur.getColumnIndex(ContactsContract.CommonDataKinds.StructuredPostal.CITY));
 	                	   System.out.println("Address is : " +City+", "+Street+", "+Postcode);
 	                	   // Get the forename and surname
 	                	   String[] separated = name.split(" ");
 	                	   System.out.println("Forename : " + separated[0]);
 	                	   System.out.println("Surname : " + separated[1]);
 	                   }
 	                   addrCur.close();

 	                   // Get Organizations.........
 	                   String orgWhere = ContactsContract.Data.CONTACT_ID + " = ? AND " + ContactsContract.Data.MIMETYPE + " = ?";
 	                   String[] orgWhereParams = new String[]{contactID,
 	                       ContactsContract.CommonDataKinds.Organization.CONTENT_ITEM_TYPE};
 	                   Cursor orgCur = cr.query(ContactsContract.Data.CONTENT_URI,
 	                               null, orgWhere, orgWhereParams, null);
 	                   if (orgCur.moveToFirst()) {
 	                       String comName = orgCur.getString(orgCur.getColumnIndex(ContactsContract.CommonDataKinds.Organization.DATA));
 	                       String title = orgCur.getString(orgCur.getColumnIndex(ContactsContract.CommonDataKinds.Organization.TITLE));
 	                       System.out.println("Title " + title);
 	                       System.out.println("Company " + comName);
 	                       //cTitle.setText(title);
 	                      // cCompany.setText(comName);
 	                       //ArrayAdapter myAdap = (ArrayAdapter) cTitle.getAdapter(); //cast to an ArrayAdapter
 	                       //int spinnerPosition = myAdap.getPosition(title);
 	                       //set the default according to value
 	                       //cTitle.setSelection(spinnerPosition);
 	                   }
 	                   orgCur.close();
     }
}
